# Laravel-Vue

Laravel Vue CRUD

# Requisitos

- https://vuejs.org/v2/guide/
- Laravel-mix

# Memoria de Desarrollo

``` bash

composer create-project --prefer-dist laravel/laravel laravel-vue

npm install

npm run dev
```